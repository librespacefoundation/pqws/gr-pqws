INCLUDE(FindPkgConfig)
PKG_CHECK_MODULES(PC_PQWS pqws)

FIND_PATH(
    PQWS_INCLUDE_DIRS
    NAMES pqws/api.h
    HINTS $ENV{PQWS_DIR}/include
        ${PC_PQWS_INCLUDEDIR}
    PATHS ${CMAKE_INSTALL_PREFIX}/include
          /usr/local/include
          /usr/include
)

FIND_LIBRARY(
    PQWS_LIBRARIES
    NAMES gnuradio-pqws
    HINTS $ENV{PQWS_DIR}/lib
        ${PC_PQWS_LIBDIR}
    PATHS ${CMAKE_INSTALL_PREFIX}/lib
          ${CMAKE_INSTALL_PREFIX}/lib64
          /usr/local/lib
          /usr/local/lib64
          /usr/lib
          /usr/lib64
)

INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(PQWS DEFAULT_MSG PQWS_LIBRARIES PQWS_INCLUDE_DIRS)
MARK_AS_ADVANCED(PQWS_LIBRARIES PQWS_INCLUDE_DIRS)

