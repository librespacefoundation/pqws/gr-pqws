title: The PQWS OOT Module
brief: GNU-Radio OOT module for decoding APRS weather messages from the
PQWS weather stations

tags: # Tags are arbitrary, but look at CGRAN what other authors are using
  - sdr
  - APRS
  - FSK
author:
  - Manolis Surligas <surligas@gmail.com>
copyright_owner:
  - Libre Space Foundation
license:
#repo: # Put the URL of the repository here, or leave blank for default
#website: <module_website> # If you have a separate project website, put it here
#icon: <icon_url> # Put a URL to a square image here that will be used as an icon on CGRAN
---
